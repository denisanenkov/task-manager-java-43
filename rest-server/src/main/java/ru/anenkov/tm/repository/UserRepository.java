package ru.anenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.User;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String>, JpaRepository<User, String> {

    @Nullable
    @Transactional(readOnly = true)
    User findByLogin(@Nullable @Param("login") String login);

    @Transactional
    void deleteUserByLogin(@Nullable @Param("login") String login);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllByLastName(@Nullable @Param("lastName") String lastName);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllByFirstName(@Nullable @Param("firstName") String firstName);

    @Nullable
    @Transactional(readOnly = true)
    List<User> findAllBySecondName(@Nullable @Param("secondName") String secondName);

}
