package ru.anenkov.tm.endpoint.soap;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @WebMethod
    public void addProject(
            @WebParam(name = "project") ProjectDTO project
    ) {
        projectService.addDTO(project);
    }

    @WebMethod
    public ProjectDTO findOneByIdEntity(
            @WebParam(name = "id") @Nullable final String id
    ) {
        return ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserId(UserUtil.getUserId(), id));
    }

    @WebMethod
    public ProjectDTO findProjectByUserIdAndId(
            @WebParam(name = "userId") @Nullable final String userId,
            @WebParam(name = "id") @Nullable final String id
    ) {
        return ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserId(userId, id));
    }

    @WebMethod
    public void removeOneById(
            @WebParam(name = "id") @Nullable final String id
    ) {
        projectService.removeProjectByUserIdAndId(UserUtil.getUserId(), id);
    }

    @WebMethod
    public void removeAllProjects() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public List<ProjectDTO> getList() {
        return ProjectDTO.toProjectListDTO(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @WebMethod
    public long count() {
        return projectService.countByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public ProjectDTO findProjectByUserIdAndName(
            @Nullable @WebParam(name = "name") final String name
    ) {
        return projectService.toProjectDTO(projectService.findProjectByUserIdAndName(UserUtil.getUserId(), name));
    }

}
