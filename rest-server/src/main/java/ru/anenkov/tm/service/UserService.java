package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.exception.empty.EmptyParameterException;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.repository.UserRepository;

import java.util.List;

@Service
public class UserService {

    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserByFirstName(@Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findAllByFirstName(firstName).get(0);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserBySecondName(@Nullable final String secondName) {
        if (secondName == null || secondName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findAllBySecondName(secondName).get(0);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findUserByLastName(@Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findAllByLastName(lastName).get(0);
    }

    @Transactional
    @Nullable
    public List<User> findAllByFirstNameREST(@Nullable final String firstName) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyParameterException();
        return userRepository.findAllByFirstName(firstName);
    }

    @Transactional
    @Nullable
    public List<User> findAllBySecondNameREST(@Nullable final String secondName) {
        if (secondName == null || secondName.isEmpty()) throw new EmptyParameterException();
        return userRepository.findAllBySecondName(secondName);
    }

    @Transactional
    @Nullable
    public List<User> findAllByLastNameREST(@Nullable final String lastName) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyParameterException();
        return userRepository.findAllByLastName(lastName);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @Transactional
    @SneakyThrows
    public void deleteUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteUserByLogin(login);
    }

    @SneakyThrows
    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @SneakyThrows
    @Transactional
    public void delete(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        userRepository.delete(user);
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Nullable
    @SneakyThrows
    @Transactional
    public User save(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        return userRepository.save(user);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return userRepository.count();
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.existsById(id);
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public List<User> findAllForREST() {
        return userRepository.findAll();
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public User findOneByIdForREST(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @SneakyThrows
    @Transactional
    public User saveUserREST(@Nullable final User user) {
        if (user == null) throw new EmptyEntityException();
        return userRepository.save(user);
    }

    @SneakyThrows
    @Transactional
    public void removeOneByIdUserREST(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @SneakyThrows
    @Transactional
    public void removeOneByLoginUserREST(
            @Nullable final String login
    ) {
        if (login == null || login.isEmpty()) throw new EmptyIdException();
        userRepository.deleteUserByLogin(login);
    }

    @SneakyThrows
    @Transactional
    public void removeAllREST() {
        userRepository.deleteAll();
    }

}
