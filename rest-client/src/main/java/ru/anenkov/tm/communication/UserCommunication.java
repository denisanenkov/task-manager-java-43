package ru.anenkov.tm.communication;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.dto.UserDTO;

import java.util.List;

@Component
public class UserCommunication {

    @Autowired
    private RestTemplate restTemplate;

    private final String URL = "http://localhost:8080/restApi";

    public List<UserDTO> users() {
        ResponseEntity<List<UserDTO>> responseEntity =
                restTemplate.exchange(URL + "/users", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<UserDTO>>() {
                        });
        List<UserDTO> userDTOS = responseEntity.getBody();
        return userDTOS;
    }

    public List<UserDTO> usersByFirstName(@Nullable final String firstName) {
        ResponseEntity<List<UserDTO>> responseEntity =
                restTemplate.exchange(URL + "/users/byFirstName/" + firstName, HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<UserDTO>>() {
                        });
        List<UserDTO> userDTOS = responseEntity.getBody();
        return userDTOS;
    }

    public List<UserDTO> usersBySecondName(@Nullable final String secondName) {
        ResponseEntity<List<UserDTO>> responseEntity =
                restTemplate.exchange(URL + "/users/bySecondName/" + secondName, HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<UserDTO>>() {
                        });
        List<UserDTO> userDTOS = responseEntity.getBody();
        return userDTOS;
    }

    public List<UserDTO> usersByLastName(@Nullable final String lastName) {
        ResponseEntity<List<UserDTO>> responseEntity =
                restTemplate.exchange(URL + "/users/byLastName/" + lastName, HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<UserDTO>>() {
                        });
        List<UserDTO> userDTOS = responseEntity.getBody();
        return userDTOS;
    }

    public UserDTO getUser(String id) {
        ResponseEntity<UserDTO> responseEntity =
                restTemplate.exchange(URL + "/users/" + id, HttpMethod.GET, null,
                        new ParameterizedTypeReference<UserDTO>() {
                        });
        UserDTO userDTOS = responseEntity.getBody();
        return userDTOS;
    }

    public void addUser(UserDTO user) {
        if (isExists(user.getId())) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<UserDTO> httpEntity = new HttpEntity<>(user, httpHeaders);
            restTemplate.exchange(URL + "/users", HttpMethod.POST,
                    httpEntity, new ParameterizedTypeReference<UserDTO>() {
                    });
        } else {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<UserDTO> httpEntity = new HttpEntity<>(user, httpHeaders);
            restTemplate.exchange(URL + "/users", HttpMethod.PUT,
                    httpEntity, new ParameterizedTypeReference<UserDTO>() {
                    });
        }
    }

    public boolean isExists(String id) {
        List<UserDTO> userList = users();
        for (UserDTO currentUser : userList) {
            if (id.equals(currentUser.getId())) return true;
        }
        return false;
    }

    public void removeOneById(String id) {
        restTemplate.delete(URL + "/users/" + id);
    }

    public long count() {
        return users().size();
    }

    public void removeOneByLogin(String login) {
        restTemplate.delete(URL + "/users/login/" + login);
    }

}
