package ru.anenkov.tm.rest;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.ApplicationContext;
import ru.anenkov.tm.communication.TaskCommunication;
import ru.anenkov.tm.configuration.MyConfiguration;
import ru.anenkov.tm.dto.TaskDTO;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskRestEndpointTest {

    private static ApplicationContext context = new AnnotationConfigApplicationContext(MyConfiguration.class);

    private static TaskCommunication communication = context.getBean("taskCommunication", TaskCommunication.class);

    @Test
    public void getListTasksTest() {
        if (communication.count() > 0) {
            List<TaskDTO> taskDTOList = new ArrayList<>();
            Assert.assertTrue(taskDTOList.isEmpty());
            List<TaskDTO> taskList = communication.tasks();
            Assert.assertFalse(taskList.isEmpty());
        }
    }

    @Test
    public void getUserByIdTest() {
        TaskDTO taskDTO = new TaskDTO("test", "test");
        taskDTO.setId(UUID.randomUUID().toString());
        communication.addTask(taskDTO);
        TaskDTO task = communication.getTask(taskDTO.getId());
        Assert.assertEquals(task.getName(), "test");
        System.out.println(task);
        communication.deleteById(task.getId());
    }

    @Test
    public void deleteByIdTest() {
        TaskDTO taskDTO = new TaskDTO("test", "test");
        taskDTO.setId(UUID.randomUUID().toString());
        communication.addTask(taskDTO);
        long countAfterAdd = communication.count();
        Assert.assertNotNull(communication.getTask(taskDTO.getId()));
        communication.deleteById(taskDTO.getId());
        long countAfterDelete = communication.count();
        Assert.assertEquals(countAfterAdd, countAfterDelete + 1);
        Assert.assertNull(communication.getTask(taskDTO.getId()));
    }

    @Test
    public void count() {
        long countBeforeAdd = communication.count();
        TaskDTO taskDTO = new TaskDTO("test", "test");
        taskDTO.setId(UUID.randomUUID().toString());
        communication.addTask(taskDTO);
        long countAfterAdd = communication.count();
        Assert.assertEquals(countBeforeAdd + 1, countAfterAdd);
        communication.deleteById(taskDTO.getId());
        long countAfterDelete = communication.count();
        Assert.assertEquals(countBeforeAdd, countAfterDelete);
    }

    @Test
    public void saveTaskTest() {
        TaskDTO taskDTO = new TaskDTO("test", "test");
        taskDTO.setId(UUID.randomUUID().toString());
        long countBeforeAdd = communication.count();
        communication.addTask(taskDTO);
        long countAfterAdd = communication.count();
        Assert.assertTrue(communication.isExists(taskDTO.getId()));
        Assert.assertNotNull(communication.getTask(taskDTO.getId()));
        Assert.assertEquals(countBeforeAdd + 1, countAfterAdd);
        communication.deleteById(taskDTO.getId());
    }

}
