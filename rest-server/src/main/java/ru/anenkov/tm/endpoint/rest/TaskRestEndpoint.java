package ru.anenkov.tm.endpoint.rest;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.api.endpoint.ITaskRestEndpoint;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @GetMapping(value = "/{projectId}/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDTO> tasks(@PathVariable @Nullable final String projectId) {
        return TaskDTO.toTaskListDTO(taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), projectId));
    }

    @Override
    @GetMapping(value = "/{projectId}/task/{id}")
    public TaskDTO getTask(
            @PathVariable @Nullable final String projectId,
            @PathVariable @Nullable final String id
    ) {
        TaskDTO task = TaskDTO.toTaskDTO(taskService.findTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id));
        System.out.println("FINDED: " + task);
        if (task == null) throw new NoSuchEntitiesException
                ("Entity \"Task\" with id = " + id + " not found!");
        return task;
    }

    @Override
    @PostMapping(value = "/task")
    public TaskDTO addTask(@RequestBody @Nullable TaskDTO task) {
        task.setUserId(UserUtil.getUserId());
        taskService.addDTO(task);
        return task;
    }

    @Override
    @PutMapping(value = "/task")
    public TaskDTO updateTask(@RequestBody @Nullable TaskDTO task) {
        task.setUserId(UserUtil.getUserId());
        taskService.addDTO(task);
        return task;
    }

    @Override
    @DeleteMapping(value = "/{projectId}/task/{id}")
    public String deleteTask(
            @PathVariable @Nullable final String projectId,
            @PathVariable @Nullable final String id
    ) {
        taskService.removeTaskByUserIdAndProjectIdAndId(UserUtil.getUserId(), projectId, id);
        return "Task with id " + id + " was deleted successfully!";
    }

    @Override
    @DeleteMapping(value = "/{projectId}/tasks")
    public void deleteAllTasks(@PathVariable @Nullable final String projectId) {
        taskService.removeAllByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

    @Override
    @GetMapping(value = "/{projectId}/tasks/count")
    public long count(@PathVariable @Nullable final String projectId) {
        return taskService.countByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

}
