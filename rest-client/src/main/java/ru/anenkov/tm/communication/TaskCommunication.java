package ru.anenkov.tm.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.dto.UserDTO;

import java.util.*;

@Component
public class TaskCommunication {

    @Autowired
    private RestTemplate restTemplate;

    private final String URL = "http://localhost:8080/restApi";

    public List<TaskDTO> tasks() {
        ResponseEntity<List<TaskDTO>> responseEntity =
                restTemplate.exchange(URL + "/tasks", HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<TaskDTO>>() {
                        });
        List<TaskDTO> taskList = responseEntity.getBody();
        return taskList;
    }

    public TaskDTO getTask(String id) {
        ResponseEntity<TaskDTO> responseEntity =
                restTemplate.exchange(URL + "/tasks/" + id, HttpMethod.GET, null,
                        new ParameterizedTypeReference<TaskDTO>() {
                        });
        TaskDTO taskDTO = responseEntity.getBody();
        return taskDTO;
    }

    public void deleteById(String id) {
        restTemplate.delete(URL + "/tasks/" + id);
    }

    public void addTask(TaskDTO task) {
        if (isExists(task.getId())) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<TaskDTO> httpEntity = new HttpEntity<>(task, httpHeaders);
            restTemplate.exchange(URL + "/tasks", HttpMethod.POST,
                    httpEntity, new ParameterizedTypeReference<TaskDTO>() {
                    });
        } else {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<TaskDTO> httpEntity = new HttpEntity<>(task, httpHeaders);
            restTemplate.exchange(URL + "/tasks", HttpMethod.PUT,
                    httpEntity, new ParameterizedTypeReference<TaskDTO>() {
                    });
        }
    }

    public boolean isExists(String id) {
        List<TaskDTO> taskList = tasks();
        for (TaskDTO currentTask : taskList) {
            if (id.equals(currentTask.getId())) return true;
        }
        return false;
    }

    public long count() {
        return tasks().size();
    }

}
