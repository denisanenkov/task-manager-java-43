
package ru.anenkov.tm.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.anenkov.tm.endpoint.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Count_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "count");
    private final static QName _CountResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "countResponse");
    private final static QName _Delete_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "delete");
    private final static QName _DeleteAll_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteAll");
    private final static QName _DeleteAllResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteAllResponse");
    private final static QName _DeleteById_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteById");
    private final static QName _DeleteByIdResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteByIdResponse");
    private final static QName _DeleteResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteResponse");
    private final static QName _DeleteUserByLogin_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteUserByLogin");
    private final static QName _DeleteUserByLoginResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "deleteUserByLoginResponse");
    private final static QName _ExistsById_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "existsById");
    private final static QName _ExistsByIdResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "existsByIdResponse");
    private final static QName _FindById_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findById");
    private final static QName _FindByIdResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findByIdResponse");
    private final static QName _FindByLogin_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findByLogin");
    private final static QName _FindByLoginResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findByLoginResponse");
    private final static QName _FindUserByFirstName_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByFirstName");
    private final static QName _FindUserByFirstNameResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByFirstNameResponse");
    private final static QName _FindUserByLastName_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByLastName");
    private final static QName _FindUserByLastNameResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserByLastNameResponse");
    private final static QName _FindUserBySecondName_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserBySecondName");
    private final static QName _FindUserBySecondNameResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "findUserBySecondNameResponse");
    private final static QName _Login_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "login");
    private final static QName _LoginResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "loginResponse");
    private final static QName _Logout_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "logout");
    private final static QName _LogoutResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "logoutResponse");
    private final static QName _Profile_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "profile");
    private final static QName _ProfileResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "profileResponse");
    private final static QName _Save_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "save");
    private final static QName _SaveResponse_QNAME = new QName("http://soap.endpoint.tm.anenkov.ru/", "saveResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.anenkov.tm.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Count }
     * 
     */
    public Count createCount() {
        return new Count();
    }

    /**
     * Create an instance of {@link CountResponse }
     * 
     */
    public CountResponse createCountResponse() {
        return new CountResponse();
    }

    /**
     * Create an instance of {@link Delete }
     * 
     */
    public Delete createDelete() {
        return new Delete();
    }

    /**
     * Create an instance of {@link DeleteAll }
     * 
     */
    public DeleteAll createDeleteAll() {
        return new DeleteAll();
    }

    /**
     * Create an instance of {@link DeleteAllResponse }
     * 
     */
    public DeleteAllResponse createDeleteAllResponse() {
        return new DeleteAllResponse();
    }

    /**
     * Create an instance of {@link DeleteById }
     * 
     */
    public DeleteById createDeleteById() {
        return new DeleteById();
    }

    /**
     * Create an instance of {@link DeleteByIdResponse }
     * 
     */
    public DeleteByIdResponse createDeleteByIdResponse() {
        return new DeleteByIdResponse();
    }

    /**
     * Create an instance of {@link DeleteResponse }
     * 
     */
    public DeleteResponse createDeleteResponse() {
        return new DeleteResponse();
    }

    /**
     * Create an instance of {@link DeleteUserByLogin }
     * 
     */
    public DeleteUserByLogin createDeleteUserByLogin() {
        return new DeleteUserByLogin();
    }

    /**
     * Create an instance of {@link DeleteUserByLoginResponse }
     * 
     */
    public DeleteUserByLoginResponse createDeleteUserByLoginResponse() {
        return new DeleteUserByLoginResponse();
    }

    /**
     * Create an instance of {@link ExistsById }
     * 
     */
    public ExistsById createExistsById() {
        return new ExistsById();
    }

    /**
     * Create an instance of {@link ExistsByIdResponse }
     * 
     */
    public ExistsByIdResponse createExistsByIdResponse() {
        return new ExistsByIdResponse();
    }

    /**
     * Create an instance of {@link FindById }
     * 
     */
    public FindById createFindById() {
        return new FindById();
    }

    /**
     * Create an instance of {@link FindByIdResponse }
     * 
     */
    public FindByIdResponse createFindByIdResponse() {
        return new FindByIdResponse();
    }

    /**
     * Create an instance of {@link FindByLogin }
     * 
     */
    public FindByLogin createFindByLogin() {
        return new FindByLogin();
    }

    /**
     * Create an instance of {@link FindByLoginResponse }
     * 
     */
    public FindByLoginResponse createFindByLoginResponse() {
        return new FindByLoginResponse();
    }

    /**
     * Create an instance of {@link FindUserByFirstName }
     * 
     */
    public FindUserByFirstName createFindUserByFirstName() {
        return new FindUserByFirstName();
    }

    /**
     * Create an instance of {@link FindUserByFirstNameResponse }
     * 
     */
    public FindUserByFirstNameResponse createFindUserByFirstNameResponse() {
        return new FindUserByFirstNameResponse();
    }

    /**
     * Create an instance of {@link FindUserByLastName }
     * 
     */
    public FindUserByLastName createFindUserByLastName() {
        return new FindUserByLastName();
    }

    /**
     * Create an instance of {@link FindUserByLastNameResponse }
     * 
     */
    public FindUserByLastNameResponse createFindUserByLastNameResponse() {
        return new FindUserByLastNameResponse();
    }

    /**
     * Create an instance of {@link FindUserBySecondName }
     * 
     */
    public FindUserBySecondName createFindUserBySecondName() {
        return new FindUserBySecondName();
    }

    /**
     * Create an instance of {@link FindUserBySecondNameResponse }
     * 
     */
    public FindUserBySecondNameResponse createFindUserBySecondNameResponse() {
        return new FindUserBySecondNameResponse();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link Profile }
     * 
     */
    public Profile createProfile() {
        return new Profile();
    }

    /**
     * Create an instance of {@link ProfileResponse }
     * 
     */
    public ProfileResponse createProfileResponse() {
        return new ProfileResponse();
    }

    /**
     * Create an instance of {@link Save }
     * 
     */
    public Save createSave() {
        return new Save();
    }

    /**
     * Create an instance of {@link SaveResponse }
     * 
     */
    public SaveResponse createSaveResponse() {
        return new SaveResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link Role }
     * 
     */
    public Role createRole() {
        return new Role();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Count }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "count")
    public JAXBElement<Count> createCount(Count value) {
        return new JAXBElement<Count>(_Count_QNAME, Count.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "countResponse")
    public JAXBElement<CountResponse> createCountResponse(CountResponse value) {
        return new JAXBElement<CountResponse>(_CountResponse_QNAME, CountResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Delete }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "delete")
    public JAXBElement<Delete> createDelete(Delete value) {
        return new JAXBElement<Delete>(_Delete_QNAME, Delete.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAll }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteAll")
    public JAXBElement<DeleteAll> createDeleteAll(DeleteAll value) {
        return new JAXBElement<DeleteAll>(_DeleteAll_QNAME, DeleteAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteAllResponse")
    public JAXBElement<DeleteAllResponse> createDeleteAllResponse(DeleteAllResponse value) {
        return new JAXBElement<DeleteAllResponse>(_DeleteAllResponse_QNAME, DeleteAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteById")
    public JAXBElement<DeleteById> createDeleteById(DeleteById value) {
        return new JAXBElement<DeleteById>(_DeleteById_QNAME, DeleteById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteByIdResponse")
    public JAXBElement<DeleteByIdResponse> createDeleteByIdResponse(DeleteByIdResponse value) {
        return new JAXBElement<DeleteByIdResponse>(_DeleteByIdResponse_QNAME, DeleteByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteResponse")
    public JAXBElement<DeleteResponse> createDeleteResponse(DeleteResponse value) {
        return new JAXBElement<DeleteResponse>(_DeleteResponse_QNAME, DeleteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteUserByLogin")
    public JAXBElement<DeleteUserByLogin> createDeleteUserByLogin(DeleteUserByLogin value) {
        return new JAXBElement<DeleteUserByLogin>(_DeleteUserByLogin_QNAME, DeleteUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "deleteUserByLoginResponse")
    public JAXBElement<DeleteUserByLoginResponse> createDeleteUserByLoginResponse(DeleteUserByLoginResponse value) {
        return new JAXBElement<DeleteUserByLoginResponse>(_DeleteUserByLoginResponse_QNAME, DeleteUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "existsById")
    public JAXBElement<ExistsById> createExistsById(ExistsById value) {
        return new JAXBElement<ExistsById>(_ExistsById_QNAME, ExistsById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "existsByIdResponse")
    public JAXBElement<ExistsByIdResponse> createExistsByIdResponse(ExistsByIdResponse value) {
        return new JAXBElement<ExistsByIdResponse>(_ExistsByIdResponse_QNAME, ExistsByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findById")
    public JAXBElement<FindById> createFindById(FindById value) {
        return new JAXBElement<FindById>(_FindById_QNAME, FindById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findByIdResponse")
    public JAXBElement<FindByIdResponse> createFindByIdResponse(FindByIdResponse value) {
        return new JAXBElement<FindByIdResponse>(_FindByIdResponse_QNAME, FindByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findByLogin")
    public JAXBElement<FindByLogin> createFindByLogin(FindByLogin value) {
        return new JAXBElement<FindByLogin>(_FindByLogin_QNAME, FindByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findByLoginResponse")
    public JAXBElement<FindByLoginResponse> createFindByLoginResponse(FindByLoginResponse value) {
        return new JAXBElement<FindByLoginResponse>(_FindByLoginResponse_QNAME, FindByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByFirstName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByFirstName")
    public JAXBElement<FindUserByFirstName> createFindUserByFirstName(FindUserByFirstName value) {
        return new JAXBElement<FindUserByFirstName>(_FindUserByFirstName_QNAME, FindUserByFirstName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByFirstNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByFirstNameResponse")
    public JAXBElement<FindUserByFirstNameResponse> createFindUserByFirstNameResponse(FindUserByFirstNameResponse value) {
        return new JAXBElement<FindUserByFirstNameResponse>(_FindUserByFirstNameResponse_QNAME, FindUserByFirstNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLastName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByLastName")
    public JAXBElement<FindUserByLastName> createFindUserByLastName(FindUserByLastName value) {
        return new JAXBElement<FindUserByLastName>(_FindUserByLastName_QNAME, FindUserByLastName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLastNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserByLastNameResponse")
    public JAXBElement<FindUserByLastNameResponse> createFindUserByLastNameResponse(FindUserByLastNameResponse value) {
        return new JAXBElement<FindUserByLastNameResponse>(_FindUserByLastNameResponse_QNAME, FindUserByLastNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserBySecondName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserBySecondName")
    public JAXBElement<FindUserBySecondName> createFindUserBySecondName(FindUserBySecondName value) {
        return new JAXBElement<FindUserBySecondName>(_FindUserBySecondName_QNAME, FindUserBySecondName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserBySecondNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "findUserBySecondNameResponse")
    public JAXBElement<FindUserBySecondNameResponse> createFindUserBySecondNameResponse(FindUserBySecondNameResponse value) {
        return new JAXBElement<FindUserBySecondNameResponse>(_FindUserBySecondNameResponse_QNAME, FindUserBySecondNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Profile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "profile")
    public JAXBElement<Profile> createProfile(Profile value) {
        return new JAXBElement<Profile>(_Profile_QNAME, Profile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "profileResponse")
    public JAXBElement<ProfileResponse> createProfileResponse(ProfileResponse value) {
        return new JAXBElement<ProfileResponse>(_ProfileResponse_QNAME, ProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Save }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "save")
    public JAXBElement<Save> createSave(Save value) {
        return new JAXBElement<Save>(_Save_QNAME, Save.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.anenkov.ru/", name = "saveResponse")
    public JAXBElement<SaveResponse> createSaveResponse(SaveResponse value) {
        return new JAXBElement<SaveResponse>(_SaveResponse_QNAME, SaveResponse.class, null, value);
    }

}
