package ru.anenkov.tm.endpoint.rest;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.service.UserService;

@RestController
@RequestMapping("/restApi")
public class UserRestEndpoint {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Nullable
    @SneakyThrows
    @GetMapping("/users")
    public Iterable<User> findAll() {
        return userService.findAll();
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/users/count")
    public long count() {
        return userService.count();
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/users/byFirstName/{firstName}")
    public Iterable<User> findAllByFirstName(
            @Nullable @PathVariable(name = "firstName") final String firstName
    ) {
        return userService.findAllByFirstNameREST(firstName);
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/users/bySecondName/{secondName}")
    public Iterable<User> findAllBySecondName(
            @Nullable @PathVariable(name = "secondName") final String secondName
    ) {
        return userService.findAllBySecondNameREST(secondName);
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/users/byLastName/{lastName}")
    public Iterable<User> findAllByLastName(
            @Nullable @PathVariable(name = "lastName") final String lastName
    ) {
        return userService.findAllByLastNameREST(lastName);
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/users/{id}")
    public User findOneById(
            @Nullable @PathVariable(name = "id") final String id
    ) {
        return userService.findOneByIdForREST(id);
    }

    @Nullable
    @SneakyThrows
    @PostMapping("/users")
    public User saveUser(
            @Nullable @RequestBody final User user
    ) {
        return userService.saveUserREST(user);
    }

    @Nullable
    @SneakyThrows
    @PutMapping("/users")
    public User updateUser(
            @Nullable @RequestBody final User user
    ) {
        return userService.saveUserREST(user);
    }

    @SneakyThrows
    @DeleteMapping("/users/{id}")
    public void deleteOneById(
            @Nullable @PathVariable(name = "id") final String id
    ) {
        userService.removeOneByIdUserREST(id);
    }

    @SneakyThrows
    @DeleteMapping("/users/login/{login}")
    public void deleteOneByLogin(
            @Nullable @PathVariable(name = "login") final String login
    ) {
        userService.removeOneByLoginUserREST(login);
    }

    @SneakyThrows
    @DeleteMapping("/users")
    public void deleteAllUsers() {
        userService.removeAllREST();
    }

}
