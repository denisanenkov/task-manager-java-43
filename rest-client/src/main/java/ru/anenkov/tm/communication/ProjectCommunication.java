package ru.anenkov.tm.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.dto.ProjectDTO;
import org.springframework.http.*;
import ru.anenkov.tm.endpoint.soap.Project;

import java.util.List;

@Component
public class ProjectCommunication {

    @Autowired
    private RestTemplate restTemplate;

    private final String URL = "http://localhost:8080/restApi";

    public List<ProjectDTO> projects() {
        ResponseEntity<List<ProjectDTO>> responseEntity =
                restTemplate.exchange(URL + "/projects",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<ProjectDTO>>() {
                        });
        List<ProjectDTO> projectList = responseEntity.getBody();
        return projectList;
    }

    public ProjectDTO getProject(String id) {
        ResponseEntity<ProjectDTO> responseEntity =
                restTemplate.exchange(URL + "/projects/" + id,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<ProjectDTO>() {
                        });
        ProjectDTO projectDTO = responseEntity.getBody();
        return projectDTO;
    }

    public void addProject(ProjectDTO project) {
        if (!isExists(project.getId())) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<ProjectDTO> httpEntity = new HttpEntity<>(project, httpHeaders);
            restTemplate.exchange(URL + "/projects", HttpMethod.POST,
                    httpEntity, new ParameterizedTypeReference<ProjectDTO>() {
                    });
        } else {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<ProjectDTO> httpEntity = new HttpEntity<>(project, httpHeaders);
            restTemplate.exchange(URL + "/projects", HttpMethod.PUT,
                    httpEntity, new ParameterizedTypeReference<ProjectDTO>() {
                    });
        }
    }

    public void addProject(Project project) {
        if (!isExists(project.getId())) {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Project> httpEntity = new HttpEntity<>(project, httpHeaders);
            restTemplate.exchange(URL + "/projects", HttpMethod.POST,
                    httpEntity, new ParameterizedTypeReference<Project>() {
                    });
        } else {
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Project> httpEntity = new HttpEntity<>(project, httpHeaders);
            restTemplate.exchange(URL + "/projects", HttpMethod.PUT,
                    httpEntity, new ParameterizedTypeReference<Project>() {
                    });
        }
    }

    public boolean isExists(String id) {
        List<ProjectDTO> projectList = projects();
        for (ProjectDTO currentProject : projectList) {
            if (id.equals(currentProject.getId())) return true;
        }
        return false;
    }

    public void deleteById(String id) {
        restTemplate.delete(URL + "/projects/" + id);
    }

    public long count() {
        return projects().size();
    }

}
