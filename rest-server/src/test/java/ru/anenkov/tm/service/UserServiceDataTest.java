package ru.anenkov.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.anenkov.tm.configuration.DataBaseConfiguration;
import ru.anenkov.tm.configuration.WebApplicationConfiguration;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.service.UserService;
import ru.anenkov.tm.service.UsersDetailsServiceBean;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class, WebApplicationConfiguration.class})
public class UserServiceDataTest {

    @Autowired
    private UsersDetailsServiceBean usersService;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private UserService userService;

    @Test
    public void createAndDeleteUserTest() {
        long countBeforeCreate = userService.count();
        usersService.create("testUser", "testUser");
        long countAfterCreate = userService.count();
        Assert.assertEquals(countBeforeCreate + 1, countAfterCreate);
        usersService.deleteUserByLogin("testUser");
        long countAfterDelete = userService.count();
        Assert.assertEquals(countBeforeCreate, countAfterDelete);
    }

    @Test
    public void showDetailsUserTest() {
        usersService.create("testUser", "testUser.1");
        UserDetails userDetails = null;
        userDetails = usersService.loadUserByUsername("testUser");
        Assert.assertEquals(userDetails.getUsername(), "testUser");
        usersService.deleteUserByLogin("testUser");
    }

    @Test
    public void findByLoginTest() {
        User user = null;
        Assert.assertNull(user);
        user = userService.findByLogin("forTests");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), "forTests");
    }

    @Test
    public void findUserByFirstNameTest() {
        User user = new User("123", "123");
        user.setFirstName("testFirstName");
        userService.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = userService.findUserByFirstName("testFirstName");
        Assert.assertNotNull(findUser);
        Assert.assertEquals("123", findUser.getLogin());
        Assert.assertEquals("testFirstName", findUser.getFirstName());
        userService.deleteById(user.getId());
    }

    @Test
    public void findUserBySecondNameTest() {
        User user = new User("123", passwordEncoder.encode("123"));
        user.setSecondName("testSecondName");
        userService.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = userService.findUserBySecondName("testSecondName");
        Assert.assertNotNull(findUser);
        Assert.assertEquals("123", findUser.getLogin());
        Assert.assertEquals("testSecondName", findUser.getSecondName());
        userService.deleteById(user.getId());
    }

    @Test
    public void findUserByLastNameTest() {
        User user = new User("123", "123");
        user.setLastName("testLastName");
        userService.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = userService.findUserByLastName("testLastName");
        Assert.assertNotNull(findUser);
        Assert.assertEquals("123", findUser.getLogin());
        Assert.assertEquals("testLastName", findUser.getLastName());
        userService.deleteById(user.getId());
    }

    @Test
    public void deleteUserByLoginTest() {
        User user = new User("123", "123");
        userService.save(user);
        Assert.assertTrue(userService.findByLogin("123") != null);
        userService.deleteUserByLogin("123");
        Assert.assertTrue(userService.findByLogin("123") == null);
    }

    @Test
    public void saveTest() {
        User user = new User("saveUserTestLogin", "saveUserTestPassword");
        long countBeforeSave = userService.count();
        userService.save(user);
        long countAfterSave = userService.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        userService.deleteUserByLogin("saveUserTestLogin");
        long countAfterDelete = userService.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

    @Test
    public void findByIdTest() {
        User user = new User("123", "123");
        userService.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = userService.findById(user.getId());
        Assert.assertNotNull(findUser);
        Assert.assertEquals("123", findUser.getLogin());
        userService.deleteById(user.getId());
    }

    @Test
    public void existsByIdTest() {
        User user = new User("123", "123");
        Assert.assertFalse(userService.existsById(user.getId()));
        userService.save(user);
        Assert.assertTrue(userService.existsById(user.getId()));
        userService.deleteById(user.getId());
        Assert.assertFalse(userService.existsById(user.getId()));
    }

    @Test
    public void countTest() {
        User user = new User("saveUserTestLogin", "saveUserTestPassword");
        long countBeforeSave = userService.count();
        userService.save(user);
        long countAfterSave = userService.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        userService.deleteUserByLogin("saveUserTestLogin");
        long countAfterDelete = userService.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

    @Test
    public void deleteByIdTest() {
        User user = new User("saveUserTestLogin", "saveUserTestPassword");
        long countBeforeSave = userService.count();
        userService.save(user);
        long countAfterSave = userService.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        userService.deleteById(user.getId());
        long countAfterDelete = userService.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

    @Test
    public void deleteTest() {
        User user = new User("deleteUserTestLogin", "deleteUserTestPassword");
        long countBeforeSave = userService.count();
        userService.save(user);
        long countAfterSave = userService.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        userService.delete(user);
        long countAfterDelete = userService.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

}
