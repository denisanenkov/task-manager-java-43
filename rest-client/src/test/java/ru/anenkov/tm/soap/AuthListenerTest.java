package ru.anenkov.tm.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.AuthEndpoint;
import ru.anenkov.tm.endpoint.soap.AuthEndpointService;
import ru.anenkov.tm.endpoint.soap.User;

import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.getListSetCookieRow;
import static ru.anenkov.tm.client.AbstractClient.setMaintain;

@Component
public class AuthListenerTest {

    @NotNull
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Nullable
    private final AuthEndpointService authEndpointService = new AuthEndpointService();

    @Nullable
    private final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    @Test
    public void login() {
        Assert.assertTrue(authEndpoint.login("forTests", "forTests"));
        Assert.assertFalse(authEndpoint.login("forTest", "forTest"));
    }

    @Test
    public void profile() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        Assert.assertNotNull(authEndpoint.profile().getLogin());
        Assert.assertNotNull(authEndpoint.profile().getPasswordHash());
        Assert.assertNotNull(authEndpoint.profile().getId());
    }

    @Test
    public void createAndDeleteUserTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        long countBeforeCreate = authEndpoint.count();
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        authEndpoint.save(user);
        long countAfterCreate = authEndpoint.count();
        Assert.assertEquals(countBeforeCreate + 1, countAfterCreate);
        authEndpoint.deleteUserByLogin("testUser");
        long countAfterDelete = authEndpoint.count();
        Assert.assertEquals(countBeforeCreate, countAfterDelete);
    }

    @Test
    public void findByLoginTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = null;
        Assert.assertNull(user);
        user = authEndpoint.findByLogin("forTests");
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), "forTests");
    }

    @Test
    public void findUserByFirstNameTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        user.setFirstName("testFirstName");
        authEndpoint.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = authEndpoint.findUserByFirstName("testFirstName");
        Assert.assertNotNull(findUser);
        Assert.assertEquals("testUser", findUser.getLogin());
        Assert.assertEquals("testFirstName", findUser.getFirstName());
        authEndpoint.deleteById(findUser.getId());
    }

    @Test
    public void findUserBySecondNameTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        user.setSecondName("testSecondName");
        authEndpoint.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = authEndpoint.findUserBySecondName("testSecondName");
        Assert.assertNotNull(findUser);
        Assert.assertEquals("testUser", findUser.getLogin());
        Assert.assertEquals("testSecondName", findUser.getSecondName());
        authEndpoint.deleteById(findUser.getId());
    }

    @Test
    public void findUserByLastNameTest() {
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        setMaintain(authEndpoint);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        user.setLastName("testLastName");
        authEndpoint.save(user);
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = authEndpoint.findUserByLastName("testLastName");
        Assert.assertNotNull(findUser);
        Assert.assertEquals("testUser", findUser.getLogin());
        Assert.assertEquals("testLastName", findUser.getLastName());
        authEndpoint.deleteById(findUser.getId());
    }

    @Test
    public void deleteUserByLoginTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        authEndpoint.save(user);
        Assert.assertTrue(authEndpoint.findByLogin("testUser") != null);
        authEndpoint.deleteUserByLogin("testUser");
        Assert.assertTrue(authEndpoint.findByLogin("testUser") == null);
    }

    @Test
    public void saveTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        long countBeforeSave = authEndpoint.count();
        authEndpoint.save(user);
        long countAfterSave = authEndpoint.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        authEndpoint.deleteUserByLogin(user.getLogin());
        long countAfterDelete = authEndpoint.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

    @Test
    public void findByIdTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        authEndpoint.save(user);
        String id = authEndpoint.findByLogin(user.getLogin()).getId();
        User findUser = null;
        Assert.assertNull(findUser);
        findUser = authEndpoint.findById(id);
        Assert.assertNotNull(findUser);
        Assert.assertEquals("testUser", findUser.getLogin());
        authEndpoint.deleteById(findUser.getId());
    }

    @Test
    public void existsByIdTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        authEndpoint.save(user);
        User findUser = authEndpoint.findByLogin(user.getLogin());
        Assert.assertTrue(authEndpoint.existsById(findUser.getId()));
        authEndpoint.deleteById(findUser.getId());
    }

    @Test
    public void countTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        long countBeforeSave = authEndpoint.count();
        authEndpoint.save(user);
        long countAfterSave = authEndpoint.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        authEndpoint.deleteUserByLogin("testUser");
        long countAfterDelete = authEndpoint.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

    @Test
    public void deleteByIdTest() {
        setMaintain(authEndpoint);
        authEndpoint.login("forTests", "forTests");
        final List<String> session = getListSetCookieRow(authEndpoint);
        Assert.assertNotNull(session);
        User user = new User();
        user.setLogin("testUser");
        user.setPasswordHash(passwordEncoder.encode("testUser"));
        long countBeforeSave = authEndpoint.count();
        authEndpoint.save(user);
        long countAfterSave = authEndpoint.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        authEndpoint.deleteById(authEndpoint.findByLogin(user.getLogin()).getId());
        long countAfterDelete = authEndpoint.count();
        Assert.assertEquals(countBeforeSave, countAfterDelete);
    }

}
