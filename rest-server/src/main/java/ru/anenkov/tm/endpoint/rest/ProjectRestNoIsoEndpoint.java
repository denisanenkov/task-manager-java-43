package ru.anenkov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.service.ProjectService;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;

import java.util.List;

@RestController
@RequestMapping("/restApi")
public class ProjectRestNoIsoEndpoint {

    @Autowired
    private ProjectService projectService;

    @PostMapping(value = "/projects")
    public Project add(@Nullable @RequestBody Project project) {
        projectService.add(project);
        return project;
    }

    @PutMapping(value = "/projects")
    public Project update(@Nullable @RequestBody Project project) {
        projectService.addDTO(ProjectDTO.toProjectDTO(project));
        return project;
    }

    @GetMapping(value = "/projects/{id}")
    public @Nullable ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") final String id) {
        return ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserIdREST(id));
    }

    @DeleteMapping(value = "/projects/{id}")
    public String removeOneById(@Nullable @PathVariable final String id) {
        ProjectDTO project = ProjectDTO.toProjectDTO
                (projectService.findProjectByIdAndUserIdREST(id));
        projectService.removeProjectByUserIdAndIdREST(id);
        return "Project with id \"" + id + "\" was deleted successfully!";
    }

    @DeleteMapping(value = "/projects")
    public void removeAllProjects() {
        projectService.removeAllByUserIdREST();
    }

    @Nullable
    @GetMapping(value = "/projects")
    public List<ProjectDTO> getListByUserId() {
        return ProjectDTO.toProjectListDTO(projectService.findAllByUserIdREST());
    }

    @GetMapping(value = "/projects/count")
    public long count() {
        return projectService.countByUserIdREST();
    }
    
}
