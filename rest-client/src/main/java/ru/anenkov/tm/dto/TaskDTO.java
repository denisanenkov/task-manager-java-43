package ru.anenkov.tm.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class TaskDTO {
 
    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";
 
    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    public TaskDTO(
            @Nullable final String name) {
        this.name = name;
    }

    public TaskDTO(
            @Nullable final String name,
            @Nullable final String description
    ) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return  "  TASK: " + "\n" +
                "  ID = '" + id + "\'," + "\n" +
                "  NAME = '" + name + "\'," + "\n" +
                "  DESCRIPTION = '" + description + "\'," + "\n" +
                "  STATUS = '" + status + "\'," + "\n" +
                "  DATE BEGIN = '" + dateBegin + "\'," + "\n" +
                "  DATE FINISH = '" + dateFinish + "\'" +  "\n\n";
    }

    public TaskDTO() {
    }

    public TaskDTO(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Status status,
            @Nullable final Date dateBegin,
            @Nullable final Date dateFinish) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

}
