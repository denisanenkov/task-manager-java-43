package ru.anenkov.tm.dto;

import org.jetbrains.annotations.NotNull;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.anenkov.tm.endpoint.soap.Role;

import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private String id = UUID.randomUUID().toString();

    private String login;

    private String passwordHash;

    private String firstName = "";

    private String secondName = "";

    private String lastName = "";

    private Set<Role> roles;

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String firstName,
            @NotNull final String secondName,
            @NotNull final String lastName
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "USER: \n" +
                "  ID = '" + id + "';\n" +
                "  LOGIN = '" + login + "';\n" +
                "  PASSWORD HASH = '" + passwordHash + "';\n" +
                "  FIRST NAME = '" + firstName + "';\n" +
                "  SECOND NAME = '" + secondName + "';\n" +
                "  ROLES = '" + roles.size() + "';\n" +
                "  LAST NAME = '" + lastName + "';\n\n";
    }

}
