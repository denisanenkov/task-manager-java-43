package ru.anenkov.tm.rest;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.anenkov.tm.communication.ProjectCommunication;
import org.springframework.context.ApplicationContext;
import ru.anenkov.tm.configuration.MyConfiguration;
import ru.anenkov.tm.dto.ProjectDTO;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRestEndpointTest {

    private static ApplicationContext context = new AnnotationConfigApplicationContext(MyConfiguration.class);

    private static ProjectCommunication communication = context.getBean("projectCommunication", ProjectCommunication.class);

    @Test
    public void getListProjectsTest() {
        if (communication.count() > 0) {
            List<ProjectDTO> projectDTOList = new ArrayList<>();
            Assert.assertTrue(projectDTOList.isEmpty());
            List<ProjectDTO> projectList = communication.projects();
            Assert.assertFalse(projectList.isEmpty());
        }
    }

    @Test
    public void getUserByIdTest() {
        ProjectDTO projectDTO = new ProjectDTO("test", "test");
        projectDTO.setId(UUID.randomUUID().toString());
        communication.addProject(projectDTO);
        ProjectDTO project = communication.getProject(projectDTO.getId());
        Assert.assertEquals(project.getName(), "test");
        System.out.println(project);
        communication.deleteById(project.getId());
    }

    @Test
    public void deleteByIdTest() {
        ProjectDTO projectDTO = new ProjectDTO("test", "test");
        projectDTO.setId(UUID.randomUUID().toString());
        communication.addProject(projectDTO);
        long countAfterAdd = communication.count();
        Assert.assertNotNull(communication.getProject(projectDTO.getId()));
        communication.deleteById(projectDTO.getId());
        long countAfterDelete = communication.count();
        Assert.assertEquals(countAfterAdd, countAfterDelete + 1);
        Assert.assertNull(communication.getProject(projectDTO.getId()));
    }

    @Test
    public void count() {
        long countBeforeAdd = communication.count();
        ProjectDTO projectDTO = new ProjectDTO("test", "test");
        projectDTO.setId(UUID.randomUUID().toString());
        communication.addProject(projectDTO);
        long countAfterAdd = communication.count();
        Assert.assertEquals(countBeforeAdd + 1, countAfterAdd);
        communication.deleteById(projectDTO.getId());
        long countAfterDelete = communication.count();
        Assert.assertEquals(countBeforeAdd, countAfterDelete);
    }

    @Test
    public void saveProjectTest() {
        ProjectDTO projectDTO = new ProjectDTO("test", "test");
        projectDTO.setId(UUID.randomUUID().toString());
        long countBeforeAdd = communication.count();
        communication.addProject(projectDTO);
        long countAfterAdd = communication.count();
        Assert.assertTrue(communication.isExists(projectDTO.getId()));
        Assert.assertNotNull(communication.getProject(projectDTO.getId()));
        Assert.assertEquals(countBeforeAdd + 1, countAfterAdd);
        communication.deleteById(projectDTO.getId());
    }

}
