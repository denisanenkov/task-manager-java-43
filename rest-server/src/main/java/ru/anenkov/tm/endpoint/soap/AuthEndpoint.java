package ru.anenkov.tm.endpoint.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.repository.UserRepository;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class AuthEndpoint {

    @Autowired
    private UserRepository userRepository;

    @Resource
    private AuthenticationManager authenticationManager;

    @WebMethod
    @SneakyThrows
    public boolean login(
            @Nullable @WebParam(name = "username") final String username,
            @Nullable @WebParam(name = "password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return authentication.isAuthenticated();
        } catch (final Exception ex) {
            return false;
        }
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userRepository.findByLogin(username);
    }

    @WebMethod
    @SneakyThrows
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public User findByLogin(
            @Nullable final @WebParam(name = "login") String login
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserByFirstName(
            @Nullable final @WebParam(name = "firstName") String firstName
    ) {
        if (firstName == null || firstName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findAllByFirstName(firstName).get(0);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserBySecondName(
            @Nullable final @WebParam(name = "secondName") String secondName
    ) {
        if (secondName == null || secondName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findAllBySecondName(secondName).get(0);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public User findUserByLastName(
            @Nullable final @WebParam(name = "lastName") String lastName
    ) {
        if (lastName == null || lastName.isEmpty()) throw new EmptyLoginException();
        return userRepository.findAllByLastName(lastName).get(0);
    }

    @SneakyThrows
    public void deleteUserByLogin(
            @Nullable final @WebParam(name = "login") String login
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteUserByLogin(login);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public User save(
            @Nullable final @WebParam(name = "user") User user
    ) {
        if (user == null) throw new EmptyEntityException();
        return userRepository.save(user);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public User findById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @WebMethod
    @SneakyThrows
    public long count() {
        return userRepository.count();
    }

    @WebMethod
    @SneakyThrows
    public void deleteById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @WebMethod
    @SneakyThrows
    public void delete(
            @Nullable final @WebParam(name = "user") User user
    ) {
        if (user == null) throw new EmptyEntityException();
        userRepository.delete(user);
    }

    @WebMethod
    @SneakyThrows
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @WebMethod
    @SneakyThrows
    public boolean existsById(
            @Nullable final @WebParam(name = "id") String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.existsById(id);
    }

}
