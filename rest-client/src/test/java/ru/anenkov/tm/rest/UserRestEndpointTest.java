package ru.anenkov.tm.rest;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.ApplicationContext;
import ru.anenkov.tm.communication.UserCommunication;
import ru.anenkov.tm.configuration.MyConfiguration;
import ru.anenkov.tm.dto.UserDTO;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRestEndpointTest {

    private static ApplicationContext context = new AnnotationConfigApplicationContext(MyConfiguration.class);

    private static UserCommunication communication = context.getBean("userCommunication", UserCommunication.class);

    @Test
    public void getListUsersTest() {
        List<UserDTO> userDTOS = new ArrayList<>();
        Assert.assertTrue(userDTOS.isEmpty());
        UserDTO userDTO1 = new UserDTO("190", "000");
        UserDTO userDTO2 = new UserDTO("390", "999");
        userDTO1.setId(UUID.randomUUID().toString());
        userDTO2.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        communication.addUser(userDTO2);
        userDTOS = communication.users();
        Assert.assertFalse(userDTOS.isEmpty());
        Assert.assertTrue(userDTOS.size() >= 2);
        communication.removeOneByLogin("190");
        communication.removeOneByLogin("390");
    }

    @Test
    public void getUserByIdTest() {
        UserDTO userDTO1 = new UserDTO("777", "000");
        userDTO1.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        UserDTO findUser = communication.getUser("777");
        communication.removeOneByLogin("777");
    }

    @Test
    public void getListUsersByFirstNameTest() {
        UserDTO userDTO1 = new UserDTO("777", "000");
        userDTO1.setFirstName(UUID.randomUUID().toString());
        userDTO1.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        List<UserDTO> userDTOS = communication.usersByFirstName(userDTO1.getFirstName());
        Assert.assertEquals(userDTOS.get(0).getFirstName(), userDTO1.getFirstName());
        communication.removeOneByLogin("777");
    }

    @Test
    public void getListUsersBySecondNameTest() {
        UserDTO userDTO1 = new UserDTO("777", "000");
        userDTO1.setSecondName(UUID.randomUUID().toString());
        userDTO1.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        List<UserDTO> userDTOS = communication.usersBySecondName(userDTO1.getSecondName());
        Assert.assertEquals(userDTOS.get(0).getSecondName(), userDTO1.getSecondName());
        communication.removeOneByLogin("777");
    }

    @Test
    public void getListUsersByLastNameTest() {
        UserDTO userDTO1 = new UserDTO("777", "000");
        userDTO1.setLastName(UUID.randomUUID().toString());
        userDTO1.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        List<UserDTO> userDTOS = communication.usersByLastName(userDTO1.getLastName());
        Assert.assertEquals(userDTOS.get(0).getLastName(), userDTO1.getLastName());
        communication.removeOneByLogin("777");
    }

    @Test
    public void saveUserTest() {
        long countBeforeSave = communication.count();
        UserDTO userDTO1 = new UserDTO("777", "000");
        userDTO1.setLastName(UUID.randomUUID().toString());
        userDTO1.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        long countAfterSave = communication.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        communication.removeOneByLogin("777");
    }

    @Test
    public void updateUserTest() {
        long countBeforeSave = communication.count();
        UserDTO userDTO1 = new UserDTO("777", "000");
        userDTO1.setLastName(UUID.randomUUID().toString());
        userDTO1.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        long countAfterSave = communication.count();
        userDTO1.setLogin("new777");
        communication.addUser(userDTO1);
        long countAfterUpdate = communication.count();
        Assert.assertEquals(countAfterSave, countAfterUpdate);
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        communication.removeOneByLogin("new777");
    }

    @Test
    public void deleteUserByIdTest() {
        long countBeforeSave = communication.count();
        UserDTO userDTO1 = new UserDTO("777", "000");
        userDTO1.setLastName(UUID.randomUUID().toString());
        userDTO1.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        long countAfterSave = communication.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        communication.removeOneById(userDTO1.getId());
        long countAfterDelete = communication.count();
        Assert.assertEquals(countAfterDelete + 1, countAfterSave);
    }

    @Test
    public void deleteUserByLoginTest() {
        long countBeforeSave = communication.count();
        UserDTO userDTO1 = new UserDTO("777", "000");
        userDTO1.setLastName(UUID.randomUUID().toString());
        userDTO1.setId(UUID.randomUUID().toString());
        communication.addUser(userDTO1);
        long countAfterSave = communication.count();
        Assert.assertEquals(countBeforeSave + 1, countAfterSave);
        communication.removeOneById(userDTO1.getId());
        long countAfterDelete = communication.count();
        Assert.assertEquals(countAfterDelete + 1, countAfterSave);
    }

}
