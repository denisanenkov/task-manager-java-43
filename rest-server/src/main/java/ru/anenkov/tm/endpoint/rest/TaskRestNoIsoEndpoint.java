package ru.anenkov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.model.Task;
import lombok.SneakyThrows;

import java.util.List;

@RestController
@RequestMapping("/restApi")
public class TaskRestNoIsoEndpoint {

    @Autowired
    TaskService taskService;
    
    @SneakyThrows
    @GetMapping(value = "/tasks/{id}")
    public Task findById(
            @Nullable @PathVariable(name = "id") final String id
    ) {
        return taskService.findById(id);
    }

    @SneakyThrows
    @DeleteMapping(value = "/tasks/{id}")
    public void deleteById(
            @Nullable @PathVariable(name = "id") final String id
    ) {
        taskService.deleteById(id);
    }

    @SneakyThrows
    @PostMapping(value = "/tasks")
    public Task save(
            @Nullable @RequestBody final Task task
    ) {
        return taskService.add(task);
    }

    @SneakyThrows
    @PutMapping(value = "/tasks")
    public Task update(
            @Nullable @RequestBody final Task task
    ) {
        return taskService.add(task);
    }

    @SneakyThrows
    @DeleteMapping(value = "/tasks")
    public void deleteAll() {
        taskService.deleteAll();
    }

    @SneakyThrows
    @GetMapping(value = "/tasks")
    public List<Task> findAll() {
        return taskService.findAll();
    }

}
